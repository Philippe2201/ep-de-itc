/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epdeitc;

/**
 *
 * @author Philippe
 */
public class TransitionMatrix {
    int[][][] transitionsMatrix;
    
    public TransitionMatrix(Automaton automaton){
        //System.out.println("----------------------------------------------------------");
        //System.out.println("Quantidade de estados: " + automaton.getnStates());
        //System.out.println("Quantidade de simbolos: " + automaton.getnSimbols());
        //System.out.println("----------------------------------------------------------");
        transitionsMatrix = new int[automaton.getnStates()][automaton.getnSimbols()][0];
        generateTransitionsMatrix(automaton);
    }
    
    private void generateTransitionsMatrix(Automaton automaton){
        int[][] transitions = automaton.getTransitions();
        
        for(int m=0; m<transitions.length; m++){
            int initialState = transitions[m][0];
            //System.out.println("Estado inicial: " + initialState);
            int simbol = transitions[m][1];
            //System.out.println("Simbolo: " + simbol);
            int finalState = transitions[m][2];
            //System.out.println("Estado final: " + finalState);
            if(simbol==0){
                //System.out.println("Transicao vazia");
                //for(int i = 1; i<automaton.getnSimbols(); i++){
                    transitionsMatrix[initialState][0] = addElement(transitionsMatrix[initialState][0], finalState);
                //}
            }else{
                transitionsMatrix[initialState][simbol] = addElement(transitionsMatrix[initialState][simbol], finalState);
            }
        }
    }
    
    private int[] addElement(int[] array, int element){
        int[] array2 = new int[array.length+1];
        System.arraycopy(array, 0, array2, 0, array.length);
        array2[array.length] = element;
        return array2;
    }
    
    public int returnState(int one, int two, int tree){
        return transitionsMatrix[one][two][tree];
    }
    
    public int[] returnStates(int one, int two){
        return transitionsMatrix[one][two];
    }
}
