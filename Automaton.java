/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epdeitc;

/**
 *
 * @author Philippe
 */
public class Automaton {
    
    private int nStates;
    private int nSimbols;
    private int nTransitions;
    private int initialState;
    private int nAcceptingStates;
    private int[] acceptingStates;
    private int[][] transitions;
    private int[][] strings;
    
    Automaton (int nStates, int nSimbols, int nTransitions, int initialState, int nAcceptingStates, int[] acceptingStates, int[][] transitions, int[][] strings){
        this.nStates = nStates;
        this.nSimbols = nSimbols;
        this.nTransitions = nTransitions;
        this.initialState = initialState;
        this.nAcceptingStates = nAcceptingStates;
        this.acceptingStates = acceptingStates;
        this.transitions = transitions;
        this.strings = strings;
    }

    /**
     * @return the nStates
     */
    public int getnStates() {
        return nStates;
    }

    /**
     * @param nStates the nStates to set
     */
    public void setnStates(int nStates) {
        this.nStates = nStates;
    }

    /**
     * @return the nSimbols
     */
    public int getnSimbols() {
        return nSimbols;
    }

    /**
     * @param nSimbols the nSimbols to set
     */
    public void setnSimbols(int nSimbols) {
        this.nSimbols = nSimbols;
    }

    /**
     * @return the nTransitions
     */
    public int getnTransitions() {
        return nTransitions;
    }

    /**
     * @param nTransitions the nTransitions to set
     */
    public void setnTransitions(int nTransitions) {
        this.nTransitions = nTransitions;
    }

    /**
     * @return the nAcceptingStates
     */
    public int getnAcceptingStates() {
        return nAcceptingStates;
    }

    /**
     * @param nAcceptingStates the nAcceptingStates to set
     */
    public void setnAcceptingStates(int nAcceptingStates) {
        this.nAcceptingStates = nAcceptingStates;
    }

    /**
     * @return the acceptingStates
     */
    public int[] getAcceptingStates() {
        return acceptingStates;
    }

    /**
     * @param acceptingStates the acceptingStates to set
     */
    public void setAcceptingStates(int[] acceptingStates) {
        this.acceptingStates = acceptingStates;
    }

    /**
     * @return the transitions
     */
    public int[][] getTransitions() {
        return transitions;
    }

    /**
     * @param transitions the transitions to set
     */
    public void setTransitions(int[][] transitions) {
        this.transitions = transitions;
    }

    /**
     * @return the strings
     */
    public int[][] getStrings() {
        return strings;
    }

    /**
     * @param strings the strings to set
     */
    public void setStrings(int[][] strings) {
        this.strings = strings;
    }

    /**
     * @return the initialState
     */
    public int getInitialState() {
        return initialState;
    }

    /**
     * @param initialState the initialState to set
     */
    public void setInitialState(int initialState) {
        this.initialState = initialState;
    }
}
