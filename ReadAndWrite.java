/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epdeitc;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

/**
 *
 * @author Philippe
 */
public class ReadAndWrite {
    
    public static ArrayList<int[]> readFile(String path){
        String read;
        ArrayList<int[]> information = null;
        LinkedList<String> linkedList = new LinkedList();
        File file = new File(path);
        FileReader reader = null;
        BufferedReader buffer = null;
        Scanner scan = null;
        try{
            reader = new FileReader(file);
            buffer = new BufferedReader(reader);
            int i = 0;
            while ((read=buffer.readLine()) != null){
                linkedList.add(read);
                i++;
            }
        }catch(IOException e){
            System.err.println(e.toString());
        }finally{

            try{
                buffer.close();
                reader.close();
            }catch(IOException e){
                System.err.println(e.toString());
            }
            
        }
        
        information = new ArrayList();
        for(String string : linkedList){
            //System.out.println("-------------------------- String: " + string);
            scan = new Scanner(string);
            int[] numbers = new int[0];
            while(scan.hasNextInt()){
                numbers = addElement(numbers, scan.nextInt());
            }
            //System.out.println("Tamanho do array que será adicionado: " + numbers.length);

            information.add(numbers);
            //for(int i = 0; i< numbers.length; i++){
            //    System.out.print(numbers[i] + " ");
            //}
            //System.out.println();
        }
        
        return information;
    }
    
    public static int[] addElement(int[] array, int element){
        //System.out.println("Elemento que vai ser adicionado: " + element);
        //System.out.println("Array recebido (tamanho = " + array.length + "):");
        //for(int i=0; i<array.length; i++){
            //System.out.print(array[i] + " ");
        //}
        //System.out.println();
        int[] newArray = new int[array.length+1];
        for(int i=0; i<array.length; i++){
            newArray[i] = array[i];
        }
        newArray[array.length] = element;
        //System.out.println("Novo array (tamanho = " + newArray.length + "):");
        //for(int i=0; i<newArray.length; i++){
            //System.out.print(newArray[i] + " ");
        //}
        //System.out.println();
        return newArray;
    }
    
    public static void writeFile(String path, String[] write){
        File file = new File(path);
        FileWriter writer = null;
        BufferedWriter buffer = null;
        
        try{
            
            writer = new FileWriter(file);
            buffer = new BufferedWriter(writer);
            for(int i=0; i<write.length; i++){
                buffer.write(write[i]);
                if(i != (write.length - 1)){
                    buffer.newLine();
                }
            }
            
        }catch(IOException e){
            System.err.println(e.toString());
        }finally{
            
            try{
                buffer.close();
                writer.close();
            }catch(IOException e){
                System.err.println(e.toString());
            }
        }
    }
}
