/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epdeitc;

import java.util.ArrayList;

/**
 *
 * @author Philippe
 */
public class Epdeitc {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String path = "/Users/Philippe/Dropbox/EP de ITC/epdeitc/src/epdeitc/teste.txt";
        String exitFile = "/Users/Philippe/Dropbox/EP de ITC/epdeitc/src/epdeitc/exit.txt";
        ArrayList<int[]> information = ReadAndWrite.readFile(path);

        Automaton[] automatons = getAutomatonInformation(information);

        runAutomaton(automatons[2]);
        
        System.out.println("Fim!");
    }
    
    //imprime a matriz de transicao
    public static void printTransitionsMatrix(){
        int[][][] transitionMatrix = transitionsMatrix.transitionsMatrix;
        for(int i=0; i<transitionMatrix.length; i++){
            for(int j=0; j<transitionMatrix[i].length; j++){
                for(int k=0; k<transitionMatrix[i][j].length; k++){
                    System.out.print(transitionMatrix[i][j][k] + " ");
                }
                System.out.print("- ");
            }
            System.out.println();
        }
    }
    
    //pega as informacoes do automato e cria um Automaton
    public static Automaton[] getAutomatonInformation(ArrayList<int[]> information) {
        Automaton[] automatons;
        //number of the current line of the information document
        int line = 0;
        //number of automaton
        int automatonAmount = information.get(line)[0];
        //System.out.println("Quantidade de automatos: " + automatonAmount);
        automatons = new Automaton[automatonAmount];
        line++;
        
        for (int i = 0; i < automatonAmount; i++) {
            //number of states of the automaton
            //System.out.println("Automato " + (i + 1) + ":");
            int nStates = information.get(line)[0];
            //System.out.println("Numero de estados: " + nStates);
            //number of simbols of the automaton
            int nSimbols = information.get(line)[1];
            //System.out.println("Numero de simbolos: " + nSimbols);
            //number of transitions of the automaton
            int nTransitions = information.get(line)[2];
            //System.out.println("Numero de transicoes: " + nTransitions);
            //initial state
            int initialState = information.get(line)[3];
            //System.out.println("Estado inicial: " + initialState);
            //number of accepting states
            int nAcceptingStates = information.get(line)[4];
            //System.out.println("Numero de estados de aceitacao: " + nAcceptingStates);
            //accepting states
            //System.out.println("linha: " + line);
            int[] acceptingStates = new int[nAcceptingStates];
            line++;
            //System.out.print("Estados de aceitacao: ");
            //create an array containing the accepting states
            for (int c = 0; c < nAcceptingStates; c++) {
                acceptingStates[c] = information.get(line)[c];
                //System.out.print(acceptingStates[c] + " ");
            }
            //System.out.println();
            
            line++;
            //create an array with the transitions
            int[][] transitions = new int[nTransitions][3];
            for (int c = 0; c < transitions.length; c++) {
                //System.out.print("Transicoes: ");
                for (int m = 0; m < transitions[c].length; m++) {
                    transitions[c][m] = information.get(line)[m];
                    //System.out.print(transitions[c][m] + " ");
                }
                //System.out.println();
                line++;
            }
            //create an array with the strings
            int nStrings = information.get(line)[0];
            //System.out.println("Numero de strings: " + nStrings);
            line++;
            int[][] strings = new int[nStrings][];
            for (int c = 0; c < nStrings; c++) {
                strings[c] = new int[information.get(line).length];
                //System.out.print("String: tamanho do array: " + information.get(line).length);
                for (int m = 0; m < strings[c].length; m++) {
                    strings[c][m] = information.get(line)[m];
                    //System.out.print(strings[c][m] + " ");
                }
                //System.out.println();
                line++;
            }
            //create the automanton
            automatons[i] = new Automaton(nStates, nSimbols, nTransitions, initialState, nAcceptingStates, acceptingStates, transitions, strings);
        }

        return automatons;
    }

    public static int[] runAutomaton(Automaton automaton) {
        int[][] strings = automaton.getStrings();
        results = new int[strings.length];
        transitionsMatrix = new TransitionMatrix(automaton);
        printTransitionsMatrix();
        
        /*System.out.println("Strings:");
        for(int i=0; i<strings.length; i++){
            for(int j=0; j<strings[i].length; j++){
                System.out.print(strings[i][j] + " ");
            }
            System.out.println();
        }*/
        
        for (int i = 0; i < results.length; i++) {
            results[i] = 0;
        }

        int currentState = automaton.getInitialState();
        System.out.println("Estado inicial do automato: " + currentState);
        for (int m = 0; m < strings.length; m++) {
            string = strings[m];
            System.out.println("-------------NOVA STRING-------------");
            System.out.println("Vai executar com a string:");
            for(int i=0; i<string.length; i++){
                System.out.print(string[i] + " ");
            }
            System.out.println();
            acceptingStatesGlobal = automaton.getAcceptingStates();
            System.out.println("Estados de aceitacao:");
            for(int j = 0; j<acceptingStatesGlobal.length; j++){
                System.out.print(acceptingStatesGlobal[j] + " ");
            }
            System.out.println();
            posicaoString = m;
            recursao(-1, currentState);
        }
        
        System.out.println("----------------------------RESULTADO---------------------------");
        for(int i=0; i<results.length; i++){
            System.out.print(results[i] + " ");
        }
        System.out.println("\n----------------------------------------------------------------");
        return results;
    }
    
    static TransitionMatrix transitionsMatrix;
    static int[] string;
    static int[] acceptingStatesGlobal;
    static int[] results;
    static int posicaoString;
    
    public static void recursao(int stringIndex, int currentState){
        System.out.println("Chamou recursao!");
        if((stringIndex+1) < string.length){
            stringIndex++;
        }else{
            //confere se o estado atual eh um estado de aceitacao ou nao
            if(searchArray(acceptingStatesGlobal, currentState)){
                System.out.println("Aceitou! stringIndex: " + stringIndex + " currentState: " + currentState);
                results[posicaoString] = 1;
                return;
            }else{
                System.out.println("Rejeitou!");
            }
        }
        
        System.out.println("Indice da String: " + stringIndex + " string.length: " + string.length);
        int currentSimbol = string[stringIndex];
        System.out.println("Simbolo Atual: " + currentSimbol);
        System.out.println("Estado Atual: " + currentState);
        
        int[] candidatosAEstadoFinal = transitionsMatrix.transitionsMatrix[currentState][currentSimbol];
        int[] epsolonEstadoAtual = transitionsMatrix.transitionsMatrix[currentState][0];

        for(int i = 0; i<candidatosAEstadoFinal.length; i++){
            currentState = candidatosAEstadoFinal[i];
            System.out.println("Foi para o estado: " + currentState);
            recursao(stringIndex, currentState);
            
            int[] epsolon = transitionsMatrix.transitionsMatrix[currentState][0];
            
            geraTransicoes(epsolon, currentState, stringIndex);
        }
        
        for(int i = 0; i<epsolonEstadoAtual.length; i++){
            currentState = epsolonEstadoAtual[i];
            System.out.println("Foi para o estado: " + currentState);
            recursao(stringIndex, currentState);
            
            int[] epsolon = transitionsMatrix.transitionsMatrix[currentState][0];
            
            geraTransicoes(epsolon, currentState, stringIndex);
        }
    }
    
    public static void geraTransicoes(int[] epsolon, int estadoAtual, int stringIndex){
        for(int i = 0; i<epsolon.length; i++){
            estadoAtual = epsolon[i];
            recursao(stringIndex, estadoAtual);
            geraTransicoes(transitionsMatrix.transitionsMatrix[estadoAtual][0], estadoAtual, stringIndex);
        }
    }
    
    public static void recursaoVazia(int[] epsolon, int estadoAtual){
        for(int i=0; i<epsolon.length; i++){
            estadoAtual = epsolon[i];
            int[] epsolon2 = transitionsMatrix.transitionsMatrix[estadoAtual][0];
            recursaoVazia(epsolon2, estadoAtual);
        }
    }
    
    /*public static void followString(int stringIndex, int currentState) {
        //System.out.println("stringIndex: " + stringIndex + " currentState: " + currentState);
        int currentSimbol = string[stringIndex];
        System.out.println("Estado atual: " + currentState + " Simbolo Atual: " + currentSimbol);
        
        if(currentSimbol == 0){
            System.out.println("A string eh vazia!");
            if(searchArray(acceptingStatesGlobal, currentState)){
                System.out.println("Aceitou!");
                results[posicaoString] = 1;
            }else{
                System.out.println("Rejeitou!");
            }
            return;
        }
        
        int[] finalStatesCandidates = transitionsMatrix.returnStates(currentState, currentSimbol);
        
        System.out.println("Candidatos a ser estado final:");
        for(int c = 0; c<finalStatesCandidates.length; c++){
            System.out.print(finalStatesCandidates[c] + " ");
        }
        System.out.println();
        
        if(finalStatesCandidates.length == 1){
            currentState = finalStatesCandidates[0];
            System.out.println("Foi para o estado: " + currentState);
            int[] transicaoVazia = transitionsMatrix.returnStates(currentState, 0);
            if(transicaoVazia.length > 0){
                for(int i = 0; i<transicaoVazia.length; i++){
                    currentState = transicaoVazia[i];
                    followString(stringIndex, currentState);
                }
            }
            stringIndex++;
            if(stringIndex<string.length){
                followString(stringIndex, currentState);
            }
        }else{
            int[] transicaoVazia = transitionsMatrix.returnStates(currentState, 0);
            if(transicaoVazia.length > 0){
                for(int i = 0; i<transicaoVazia.length; i++){
                    currentState = transicaoVazia[i];
                    followString(stringIndex, currentState);
                }
            }
            stringIndex++;
            for(int i=0; i<finalStatesCandidates.length; i++){
                System.out.println("Tem mais de uma possibilidade");
                System.out.println("Testando o estado " + finalStatesCandidates[i]);
                currentState = finalStatesCandidates[i];
                if(stringIndex < string.length){
                    //System.out.println("Chegou ao final da string");
                    followString(stringIndex, currentState);
                }
            }
        }
        
        if(searchArray(acceptingStatesGlobal, currentState)){
            System.out.println("Aceitou!");
            results[posicaoString] = 1;
        }else{
            System.out.println("Rejeitou!");
        }
    }*/
    
    public static boolean searchArray(int[] array, int element){
        for(int i = 0; i<array.length; i++){
            if(array[i] == element){
                return true;
            }
        }
        return false;
    }
}