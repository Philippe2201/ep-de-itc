/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package epdeitc;

import java.util.ArrayList;

/**
 *
 * @author Philippe
 */
public class BinaryMatrix {

    ArrayList<int[]> binaryMatrix;

    public BinaryMatrix(int size) {
        binaryMatrix = new ArrayList();
        generateMatrix(size);
        
        for(int[] array : binaryMatrix){
            for(int i = 0; i<array.length; i++){
                System.out.print(array[i]);
            }
            System.out.println(" Soma: " + calculateSum(array));
        }
    }

    /*
     * Metodo que permuta os elementos em um array
     * @param matrix - matriz com os elementos
     * @param amount - quantidade de 1's na matriz
     * @param start - onde esses 1's comecam
     */
    private void recurcao(int[] matrix, int amount, int start) {
        if (amount == 0) {
            return;
        }
        for (int i = (amount - 1 + start); i < (matrix.length); i++) {
            int[] recordMatrix = new int[matrix.length];
            System.arraycopy(matrix, 0, recordMatrix, 0, matrix.length);
            binaryMatrix.add(recordMatrix);
            if (i == (matrix.length - 1)) {
                return;
            }
            int temp = matrix[i + 1];
            matrix[i + 1] = matrix[i];
            matrix[i] = temp;
        }
    }

    /*
     * Metodo que gera a matriz de numeros binarios usados para mapear os estados
     * @param size - tamanho da matriz (quantidade de estados)
     */
    private void generateMatrix(int size) {
        int matrix[] = new int[size];

        for (int quantidade = 0; quantidade < (matrix.length + 1); quantidade++) {
            if (quantidade == 0) {
                for (int i = 0; i < matrix.length; i++) {
                    matrix[i] = 0;
                }
                int[] recordMatrix = new int[matrix.length];
                System.arraycopy(matrix, 0, recordMatrix, 0, matrix.length);
                binaryMatrix.add(recordMatrix);
            } else {
                for (int vez = 0; vez < (matrix.length - quantidade + 1); vez++) {
                    for (int i = 0; i < vez; i++) {
                        matrix[i] = 0;
                    }
                    for (int i = vez; i < matrix.length; i++) {
                        if (i < (quantidade + vez)) {
                            matrix[i] = 1;
                        } else {
                            matrix[i] = 0;
                        }
                    }
                    if (quantidade != 1) {

                        recurcao(matrix, quantidade, vez);

                    } else {
                        int[] recordMatrix = new int[matrix.length];
                        System.arraycopy(matrix, 0, recordMatrix, 0, matrix.length);
                        binaryMatrix.add(recordMatrix);
                    }
                }
            }
        }
    }
    
    public int calculateSum(int[] matrix){
        int sum = 0;
        for(int i = 0; i<matrix.length; i++){
            if(matrix[i] == 1){
                sum += (int) Math.pow(2, (matrix.length-i-1));
            }
        }

        return sum;
    }

    public int returnPosition(int state1, int state2){
        return 0;
    }
}
